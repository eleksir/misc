#!/usr/bin/perl
#
use strict;
use warnings "all";

use DBD::mysql;

my $dsn = 'DBI:mysql:wordpress:localhost';
my $db_user_name = 'root';
my $db_password = '';
my ($id, $password);
my $dbh = DBI->connect($dsn, $db_user_name, $db_password);
$dbh->{'mysql_enable_utf8'} = 1;
$dbh->do('set names utf8');

my $sth = $dbh->prepare(qq{
SELECT ID, post_date, post_content, post_title FROM wp_posts
    });
$sth->execute();

my $PWD = $ENV{'PWD'};
my @posts;
my %POSTS;
while (my ($id, $post_date, $post_content, $post_title) = $sth->fetchrow_array())
{
	push @posts, $id;
	chdir($PWD);
	unless(-d "wordpress")
	{
		mkdir "wordpress";
	}
	unless(-d "wordpress/$id")
	{
		mkdir "wordpress/$id";
	}
	chdir("wordpress/$id");
	open(POST, ">post_content");
	print POST "$post_content";
	close POST;
	open(TITLE, ">post_title");
	print TITLE "$post_title";
	close TITLE;
	open(DATE, ">post_date");
	print DATE "$post_date";
	close DATE;
	$POSTS{$id} = <<DATA;
Date: $post_date
Title: $post_title
Status: public

$post_content
DATA

}

$sth->finish();
foreach my $post (@posts){
	print "POST: $post\n";
	$sth = $dbh->prepare(qq{
		SELECT `term_taxonomy_id` FROM `wp_term_relationships` WHERE `object_id`=$post;
	});
	$sth->execute();
	my @tags;
	while(my ($tag) = $sth->fetchrow_array()){
		push @tags, $tag;
	}
	$sth->finish();
	print "TAGS: ";
       	print join(',', @tags);
	print "\n";

	$sth = $dbh->prepare(qq{
		 select name from `wp_terms` where `term_id` in (SELECT `term_taxonomy_id` FROM `wp_term_relationships` WHERE `object_id`=$post);
	});
	$sth->execute();
	my @categories;
	while(my ($category) = $sth->fetchrow_array()){
		next if($category eq 'Uncategorized');
		push @categories, $category;
	}
	$sth->finish();
	my $cat = '';
	my $category;
	foreach $category (@categories){
		$cat .= $category . ', ';
	}
	chop($cat);chop($cat);
	chdir($PWD);
	chdir("wordpress/$post");
	open(CAT, ">post_tags");
	print CAT $cat;
	close CAT;
	open(P, ">/$post.md");
	print P "Tags: $cat
$POSTS{$post}";
}
__END__

query list of categories for post id
select name from `wp_terms` where `term_id` in (SELECT `term_taxonomy_id` FROM `wp_term_relationships` WHERE `object_id`=501);

