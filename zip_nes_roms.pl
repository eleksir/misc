#!/usr/bin/perl
#
# this script conpress all *.nes files in current dir' subdirs with zip
#

use strict;
use warnings "all";
use IO::Compress::Zip qw(:all);

sub zlist(@);

my $L1;
opendir($L1, ".") || die;
my @L1DIR = readdir($L1);
closedir($L1);
undef $L1;

foreach my $l1dir (@L1DIR) {
	next if($l1dir eq ".");
	next if($l1dir eq "..");

	if (-d $l1dir) {
		chdir ($l1dir);
		my @list;
		opendir DIR, "." || die;

		while (readdir(DIR)){
			if ((-f $_) and (($_ =~ /nes$/) or ($_ =~ /unf$/))){ push @list, $_; }
		}

		closedir (DIR);
		zlist(@list);
		@list = -1; undef @list;
		chdir("..");
	}
}

exit 0;

sub zlist(@){
	my @list = @_;
	my $item;

	foreach $item (@list) {
		printf ("zipping: %s", $item);
		zip [$item] => "$item.zip", -Level => Z_BEST_COMPRESSION, Minimal => 1, TextFlag => 0, BinModeIn => 1, AutoClose => 1, CanonicalName => 1;

		if(length($ZipError) > 0){
			printf (" Warning: unable to zip %s - %s\n", $item, $ZipError);
		}else{
			printf (" and deleting source file\n");
			unlink($item);
		}

	}
}
