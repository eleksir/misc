#!/usr/bin/perl
# finds and copies .zip to numeric subdirs

use strict;
use warnings "all";
use File::Copy;


my $file;
my $counter = 1;
my @list;
my $curdir = 1;
mkdir ($curdir);
opendir (DIR, ".");

foreach $file (readdir(DIR)){
	next unless ($file =~ /\.zip$/);

	if($counter == 5000){
		$counter = 1;
		$curdir = $curdir + 1;
		mkdir ($curdir);
	}

	my $nefile = $curdir .'/'.$file;
	print "$file $nefile\n";
	move($file, $nefile) or die "$!";
	$counter = $counter + 1;
}

closedir (DIR);

