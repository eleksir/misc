#!/usr/bin/perl
#
# this script compress all *.fb2 files in current directory with zip
#

use strict;
use warnings "all";
use Archive::Zip qw(:ERROR_CODES :CONSTANTS);

sub zlist(@);

my @list;
my @dirlist;
opendir DIR, "." || die;
@dirlist = readdir(DIR);
closedir(DIR);

foreach (@dirlist){
	if ((-f $_) and ($_ =~ /\.fb2/)){ push @list, $_; }
}

@dirlist = -1; undef @dirlist;
zlist(@list);
exit 0;

sub zlist(@){
	my @list = @_;
	my $item;

	foreach $item (@list)  {
		my $zip = Archive::Zip->new();
		Archive::Zip::setChunkSize( 262144 );
		my $zippy = $zip->addFile($item);

		if (defined($zippy)) {
			$zippy->desiredCompressionMethod( COMPRESSION_DEFLATED );
			$zippy->desiredCompressionLevel( COMPRESSION_LEVEL_BEST_COMPRESSION );

			if ( $zip->writeToFileNamed("$item.zip") == AZ_OK ) {
				unlink($item);
				syswrite STDOUT, ".";
			} else {
				printf ("Warning: unable to write to %s.zip\n", $item);
			}
		} else {
			printf ("Warning: unable to add %s to zip-file\n", $item);
		}

		undef $zip;
		undef $zippy;
	}

	undef $item;
	@list = -1; undef @list;
}
