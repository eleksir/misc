#!/usr/bin/perl
#
# slow implementation of recursive dir listing
#

use strict;
use warnings "all";

sub getfilenames;
sub help;

help unless defined($ARGV[0]);

my @files = ();
getfilenames($ARGV[0]);
my $file = "";

while($file = pop (@files)){
        print "$file\n";
}

exit 0;

sub getfilenames {
    my $path    = shift;

    opendir (DIR, $path) or return "";
    my @localfiles;
    @localfiles =
        map { $path . '/' . $_ }
        grep { !/^\.{1,2}$/ }
        readdir (DIR);
    @files = (@files, @localfiles);

    foreach (@localfiles) {
            next if(-l $_);
            if( -d $_){
                getfilenames($_);
            }
    }

}

sub help {
        print "Usage: $0 dir\n";
        exit 0;
}
