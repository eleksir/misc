#!/usr/bin/env perl

use strict;
use warnings "all";

use Net::DNS;

my $max_sleep = 2000;         # max sleep between sequental lookups 2 seconds long
my $max_burst_lookups = 15;   # max amount of lookup without delay

my $domfile = 'domfile.txt';  # file format: domainname record_type number_of_values
                              # num_of_values can be -1 if no A record exist

sub lookup;     # hotpath :)
sub nodomlist;
sub usleep;     # millisecond sleep, 1000 milliseconds = 1 second

nodomlist() unless ( -f $domfile );

my @list;
open (LIST, $domfile) or die ("Unable to read $domfile");

while (<LIST>){
	next if (($_ eq '') or ($_ =~ /^\s*#/));
	chomp($_); chomp($_);
	push @list, $_;
}

close LIST;

my $res = Net::DNS::Resolver->new(
	nameservers => [ '192.168.88.2' ],
	recurse     => 1,
	debug       => 0
);

my $lookup_cnt = int(rand($max_burst_lookups));
while ( 1 ) {
	my $ok = 0; my $error = 0;
	for (my $i = 0; $i < 10000; $i++) {
		if ($lookup_cnt <= 0){
			usleep(int(rand($max_sleep)));
			$lookup_cnt = int(rand($max_burst_lookups));
		} else {
			$lookup_cnt--;
		}
		my ($domain, $record_type, $amount) = split(/\s+/, $list[int(rand(@list))]);
		my $result = lookup($domain, $record_type);

		if (defined($result) and ($result == $amount)){ $ok++; }
		elsif (($amount == -1) and ($result == undef)){ $ok++; }
		else { $error++; }
	}

	print "Success/Error: $ok/$error\n";
}

exit 0;


sub lookup {
	my $host = shift;
	my $type = shift;
	my $rec_count = 0;
	my $reply = $res->query($host, $type);

	if (defined($reply)) {

		foreach my $rr ($reply->answer) {
			$rec_count++ if ($rr->type eq $type);
		}

		return $rec_count;
	} else {
#		warn "query failed: ", $res->errorstring, "\n";
		return undef;
	}
}

sub nodomlist {
	print "Unable to find $domfile\n";
	exit 1;
}

sub usleep {
	my $delay = shift;
	select(undef, undef, undef, $delay/1000);
	return;
}
