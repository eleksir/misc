#!/usr/bin/perl
#
# this script conpress all *.fb2 files in current directory with zip
#

use strict;
use warnings "all";
use IO::Compress::Zip qw(:all);

sub zlist(@);

my @list;
opendir DIR, "." || die;

while (readdir(DIR)){
	if ((-f $_) and ($_ =~ /\.fb2/)){ push @list, $_; }
}

closedir (DIR);

zlist(@list);

exit 0;

sub zlist(@){
	my @list = @_;
	my $item;

	foreach $item (@list)  {
#		printf ("zipping: %s to %s.zip", $item, $item);
		zip [$item] => "$item.zip", -Level => Z_BEST_COMPRESSION, Minimal => 1, TextFlag => 1, BinModeIn => 1, AutoClose => 1, CanonicalName => 1;

		if(length($ZipError) > 0){
			printf ("Warning: unable to zip %s - %s\n", $item, $ZipError);
		}else{
#			printf (" and deleting source file\n");
			unlink($item);
		}

	}
}

