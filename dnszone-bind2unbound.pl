#!/usr/bin/env perl

use warnings "all";
use strict;

use JSON::PP;
use DNS::ZoneParse;

sub process_zone;

my $conf_file = 'dnszone-bind2unbound.json';

open (CONF, "$conf_file") || die "unable to read $conf_file";
my $sep = $/; undef $/;
my $conf = <CONF>;
close CONF;
$/ = $sep; undef $sep;

$conf = decode_json $conf;
my %CONF = %$conf;
undef $conf;

# теперь у нас есть хэш с названиями зон и путями до файлов
foreach my $zone_name (keys(%CONF)){
	process_zone($zone_name, $CONF{$zone_name});
}

exit 0;

sub process_zone {
	my ($name, $file) = @_;
	my $zonefile = DNS::ZoneParse->new($file, $name);
	my $zone = $zonefile->dump; # ref to hash
	foreach my $record(@{${$zone}{NS}}){
		print "$record->{name} $record->{ttl} $record->{class} NS $record->{host}\n";
	}
	foreach my $record(@{${$zone}{A}}){
		print "$record->{name} $record->{ttl} $record->{class} A $record->{host}\n";
	}
        foreach my $record(@{${$zone}{AAAA}}){
		print "$record->{name} $record->{ttl} $record->{class} AAAA $record->{host}\n";
	}
        foreach my $record(@{${$zone}{CNAME}}){
		print "$record->{name} $record->{ttl} $record->{class} CNAME $record->{host}\n";
	}
}
