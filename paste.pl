#!/usr/bin/perl

use strict;
use warnings "all";
use Fcntl;

my $str = '';

my $DIR = '/tmp/paste';
my $fsize = 262144;

# assume GET request
if (defined($ARGV[0])){
	if ($ARGV[0] eq 'e'){
		$str = 'Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain
Content-Disposition: attachment; filename="e";

#!/bin/bash

usage() {
description | fmt -s >&2
}

description() {
cat << HERE

DESCRIPTION
  Upload data and fetch URL from the pastebin http://p.dafuq.ru/

USAGE
  $0 filename.txt
  $0 text string
  $0 < filename.txt
  piped_data | $0
 
NOTES
--------------------------------------------------------------------------
* INPUT METHODS *
$0 can accept piped data, STDIN redirection [<filename.txt], text strings following the command as arguments, or filenames as arguments.  Only one of these methods can be used at a time, so please see the note on precedence.  Also, note that using a pipe or STDIN redirection will treat tabs as spaces, or disregard them entirely (if they appear at the beginning of a line).  So I suggest using a filename as an argument if tabs are important either to the function or readability of the code.

* PRECEDENCE *
STDIN redirection has precedence, then piped input, then a filename as an argument, and finally text strings as an arguments.

  EXAMPLE:
  echo piped | "$0" arguments.txt < stdin_redirection.txt
 
In this example, the contents of file_as_stdin_redirection.txt would be uploaded. Both the piped_text and the file_as_argument.txt are ignored. If there is piped input and arguments, the arguments will be ignored, and the piped input uploaded.

* FILENAMES *
If a filename is misspelled or doesn\'t have the necessary path description, it will NOT generate an error, but will instead treat it as a text string and upload it.
--------------------------------------------------------------------------

HERE
exit
}

if [ -t 0 ]; then
  echo Running interactively, checking for arguments... >&2
  if [ "$*" ]; then
    echo Arguments present... >&2
    if [ -f "$*" ]; then
      echo Uploading the contents of "$*"... >&2
      cat "$*"
    else
      echo Uploading the text: \""$*"\"... >&2
      echo "$*"
    fi | curl -F "exs=<-" http://p.dafuq.ru/
  else
    echo No arguments found, printing USAGE and exiting. >&2
    usage
  fi
else
  echo Using input from a pipe or STDIN redirection... >&2
  while read -r line ; do
    echo $line
  done | curl -F "exs=<-" http://p.dafuq.ru/
fi
';
		syswrite STDOUT, $str;
		exit 0;
	}
	if ($ARGV[0] eq 'd'){
		$str = 'Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain
Content-Disposition: attachment; filename="d";

#!/usr/bin/env bash

if [[ "$1" == "--help" ]]; then
	echo "Usage:  $0 < file"
        echo "        echo foo | $0"
elif [[ -f "$1" ]]; then
	curl -F "exs=<-" "http://p.dafuq.ru" <"$1"
else
	curl -F "exs=<-" "http://p.dafuq.ru"
fi

exit 0
';
		syswrite STDOUT, $str;
		exit 0;
	}
	if (-f "$DIR/$ARGV[0]") {
		$str .= 'Status: 200 OK
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-Type: text/plain; charset=UTF-8

';

		open DATA, "$DIR/$ARGV[0]";
		while(<DATA>){
			$str .= $_;
		}
		close DATA;
	}else{
		$str .= "Status: 404 Not Found
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

http://p.dafuq.ru/$ARGV[0] Not found.
";
	}

# assume POST(?) request
}else{
	if ((defined($ENV{'CONTENT_LENGTH'})) and $ENV{'CONTENT_LENGTH'} < $fsize){
		my $host;

		if ((defined $ENV{'HTTP_HOST'})){
			$host = $ENV{'HTTP_HOST'};
		} elsif (defined ($ENV{'SERVER_NAME'})){
			$host = $ENV{'SERVER_NAME'};
		} else {
# ??? No required environment variables, this script intended to run under proper http/cgi
			$str = 'Status: 400 Bad Request
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

';
		}

		my $fname;

		for(my $i = 0;$i < 3; $i++){
			$fname .= ('a'..'z', 'A'..'Z')[int rand length(join('',('a'..'z', 'A'..'Z')))];
		}

		my $c = 0;
		my $delim = '';
		my $header = '';
		my $char = '';

		while(read(STDIN, $char, 1)){
		# looks like this case is obviously sic, but... :)
			if($c >= $ENV{'CONTENT_LENGTH'}){
				$str = 'Status: 400 Bad Request
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

';
			}
			last if ($char eq "\n");
			$delim .= $char;
			$c++;
		}

		read(DATA, $char, 1);

		while(read(STDIN, $char, 1)){
			if($c >= $ENV{'CONTENT_LENGTH'}){
				$str = 'Status: 400 Bad Request
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

';
			}

			last if ($char eq "\n");
			$header .= $char;
			$c++;
		}

		unless ($header =~ /Content-Disposition: form-data; name=.exs./) {
			$str = 'Status: 400 Bad Request
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

Form should be multipart and parameter/field should be exs.';
			syswrite STDOUT, $str;
			exit 0;
		}

		read(STDIN, $char, 2);

		my $datalen = $ENV{'CONTENT_LENGTH'} - (length($delim) + 2  + length($header) + 4 + length($delim) + 3);

		my $paste = '';
		read(STDIN, $paste, $datalen);
		
		# read one nl+cr
		read(STDIN, $char, 1);

		# read last delimiter
		my $checkstr;
		read(STDIN, $checkstr, (length($delim)));

		# compare first delimiter and last one
		chop($delim); chop($checkstr);

		unless ($delim eq $checkstr){
			$str .= 'Status: 400 Bad Request
Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

';
		}

		sysopen(DATA, "$DIR/$fname", O_WRONLY|O_CREAT|O_TRUNC);
		syswrite DATA, $paste;
		close(DATA);

		$str = "Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/plain

http://$host/$fname
";

	}else{
		$str .= 'Pragma: no-cache
Cache-Control: no-cache
Content-Language: en, ru
Content-type: text/html; charset=UTF-8

<!DOCTYPE html>
<html lang="ru">
	<head>
		<style>
			body {
				background-color: #adc0fc;
				text-align: center;
			}
			textarea {
				background-image: url("http://img.dafuq.ru/bg_logo.png");
				background-repeat: no-repeat;
				background-position: center center;
				width: 90%;
				height: 70vh;
			}
		</style>
		<title>Slack paste.</title>
	</head>
	<body>

	<h3>Slack Paste</h3>
	<form method="post" action="/" enctype="multipart/form-data">
		<textarea name="exs" id="textbox" autofocus></textarea><br />
		<input type="submit" value="Post it!" style="font-size:115%;" />
	</form>
	<p style="font-size: small;">Console client can be found here: <a href="http://p.dafuq.ru/e">http://p.dafuq.ru/e</a>&nbsp; &nbsp; thanks <a href="http://sprunge.us">sprunge.us</a> for their client :)<br/>
	More minimalist console client can be found here: <a href="http://p.dafuq.ru/d">http://p.dafuq.ru/d</a><br/>
	<br/>
	2015</p>
	</body>
</html>
';
	}
}

syswrite STDOUT, $str;

__END__
