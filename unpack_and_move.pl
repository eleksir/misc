#!/usr/bin/perl

# finds in current dir 7z compressed file, makes dir according filename,
# move file to that dir, unpack file to that dir, remove file
use warnings "all";
use strict;

use File::Copy;

my $sevenzip = '7z';

if ($^O =~ /MSWin32/) {
	if (-f "$ENV{'ProgramFiles'}/7-Zip/7z.exe") {
		$sevenzip = "$ENV{'ProgramFiles'}/7-Zip/7z.exe";
	} elsif (-f "$ENV{'ProgramFiles(x86)'}/7-Zip/7z.exe") {
		$sevenzip = "$ENV{'ProgramFiles(x86)'}/7-Zip/7z.exe";
	} else {
		print "no 7-zip found in program files\n";
		exit 1;
	}
}

opendir(S, ".");
my @list = readdir(S);

foreach (@list) {
	next if ($_ eq ".");
	next if($_ eq "..");
	my $filename = $_;
	my $name = (split(/\.7z/, $filename))[0];
	next unless (mkdir($name));
	next unless ( move ($filename, $name) == 1);
	next unless (chdir($name));
	system($sevenzip, "x", $filename);
	unlink($filename);
	chdir("..");
}
