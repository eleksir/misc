#!/usr/bin/perl

use strict;
use warnings "all";

use XML::Simple;
use bytes;

my $PROGRAM_NAME = $0;

my $xml = `curl -s -q 'https://pogoda.yandex.ru/static/cities.xml' >/tmp/cities.xml`;
if ($? != 0){
	print "Network error, quitting.";
	exit 1;
}

sub getIdByCountryAndCity(@); # supply Country and City in RUSSIAN NOTATION !
sub usage();

usage() unless (defined ($ARGV[0]) and ($ARGV[0] ne ''));
usage() unless (defined ($ARGV[1]) and ($ARGV[1] ne ''));

my $id = getIdByCountryAndCity($ARGV[0], $ARGV[1]);
if (defined($id)){
	printf("%s\n", $id);
}else{
	print "Error - no such id @ yandex weather xml data list\n";
	exit 1;
}
exit 0;

sub getIdByCountryAndCity(@){
	my ($country, $city) = @_;
	my $id = undef;
	my $simple = XML::Simple->new( KeyAttr => '' );
	my $ref = $simple->XMLin('/tmp/cities.xml');
	foreach (@{$ref->{country}}){
		my %hash = %$_;
		my $a = sprintf("%s", $hash{'name'});
		next unless ($a eq $country);
		foreach(@{$hash{'city'}}){
			next unless(${$_}{'content'} eq $city);
			$id = ${$_}{'id'};
                        last;
		}
	}
	return $id;
}

sub usage(){
	print <<EOF;
yagecibycocin
Get yandex city id by country name and city name. 
usage: $PROGRAM_NAME Country City
  if country name or city consists of multiple words, it must be enclosed with quotation
       $PROGRAM_NAME \"Country Name\" \"City Name\"

Script understands country name and city name exactly as they appear at
http://weather.yandex.ru/static/cities.xml
EOF
	exit 0;
}
