#!/usr/bin/env perl
# dns shooting utility

use strict;
use warnings "all";

use Net::DNS;

my $record_type = 'TXT';
my $record_amount = 28;
my $domain = 'cnn.com';

sub lookup;

my $res = Net::DNS::Resolver->new(
	nameservers => [ '217.16.16.13', '217.16.20.13', '217.16.16.12', '217.16.22.12' ],
	recurse     => 1,
	debug       => 0
);

my $i = 0;
while ( 1 ) {
	my $hit = 0; my $miss = 0;

	for ($i = 0; $i < 1000; $i++) {
		if (lookup($domain) == 1){ $miss++; }
		else { $hit++; }
	}

	print "hit/miss: $hit/$miss\n";
}
exit 0;

sub lookup {
	my $host = shift;
	my $reply = $res->query($host, $record_type);

	if (defined($reply)) {
		my $rec_count = 0;

		foreach my $rr ($reply->answer) {
			next if ($rr->type ne $record_type);
			$rec_count++;
		}

		return 1 if ($rec_count < $record_amount);
		return 0;
	} else {
#		warn "query failed: ", $res->errorstring, "\n";
		return 1;
	}
}

